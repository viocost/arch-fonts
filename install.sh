#!/bin/bash

cd fonts
unzip fonts.zip 
rm fonts.zip
sudo cp -r ./* /usr/share/fonts
sudo fc-cache -fv

echo Fonts installed successfully!
